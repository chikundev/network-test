-- chikun :: 2014
-- Networking test: Server

-- Require socket libraries
local socket = require "socket"

-- UDP functions
local udp = socket.udp()

-- Continue server loop?
local loop = true

-- Set timeout and port number
udp:settimeout(0)
udp:setsockname("*", 3564)

-- Next entity
local nextEntity = 1

-- Table of entities
local entities = { }

-- Main server loop
function loopFunction()

    -- Receive data
    data, msg_or_ip, port_or_nil = udp:receivefrom()

    -- If data has come through
    if data then

        -- Figure out command and parameters
        cmd, entity, parms = data:match("^(%S*) (%S*) (.*)")

        -- If client wants update
        if cmd == "update" then

            for k, v in ipairs(entities) do

                if v.id ~= entity then

                    udp:sendto("move " .. v.id .. " " .. v.x .. " " ..
                        v.y, msg_or_ip, port_or_nil)

                end

            end

        -- If client wants ID
        elseif cmd == "getID" then

            -- Send new entity ID
            udp:sendto("setID " .. nextEntity, msg_or_ip, port_or_nil)

            -- Add entity to table
            table.insert(entities, {
                    id = tostring(nextEntity),
                    x = 32,
                    y = 32
            })

            -- Report that ID has been delegated
            print("Delegated ID ".. nextEntity .. " to " ..
                msg_or_ip .. ":" .. port_or_nil)

            -- Increase entity number
            nextEntity = nextEntity + 1

        -- If client is sending movement data
        elseif cmd == "move" then

            for k, v in ipairs(entities) do

                if v.id == entity then

                    local newX, newY =
                        parms:match("(.*) (.*)")

                    v.x = tonumber(newX)
                    v.y = tonumber(newY)

                    print("Moved " .. entity .. " to (" ..
                        newX .. ", " .. newY .. ")")

                end

            end

        end

    end

end
