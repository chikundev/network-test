#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

int updateLua(lua_State *L);

int main() {

    /* Initialisation of variables */
    lua_State *L;
    clock_t sysClock;
    double lastTime, dt;
    int keepLooping;

    /* Print that shit's about to get real */
    printf("Engage server overdrive\n");

    /* Lua loading functions */
    L = luaL_newstate();
    luaL_openlibs(L);
    luaL_dofile(L, "server.lua");

    /* Set last time */
    sysClock = clock();
    lastTime = sysClock;

    /* Game will loop while this is true */
    keepLooping = 1;

    while (keepLooping) {

        /* Update clock */
        sysClock = clock();

        /* Update Lua */
        keepLooping = updateLua(L);

        /* Only perform these functions when dt != 0 */
        if (lastTime != sysClock) {

            /* Update dt you ravenous dog. */
            dt = ((double) sysClock - lastTime) / 1000;
        }

        /* Update previous time */
        lastTime = sysClock;

    }

    /* Print that shit is over */
    printf("Disengage server underdrive\n");

    /* Tell the system we cool */
    return 0;

}


/* Run our Lua loop */
int updateLua(lua_State *L) {

    /* For catching Lua errors */
    int errorCode;

    /* Set Lua function to call */
    lua_getglobal(L, "loopFunction");

    /* Call function and catch errors */
    errorCode = lua_pcall(L, 0, 0, 0);

    /* If error code happens... */
    if (errorCode != 0) {

        /* Print said error code */
        printf("%s\n", lua_tostring(L, -1));

        /* Return that there was an error */
        return 0;

    }

    /* Return that there was NO error */
    return 1;

}
