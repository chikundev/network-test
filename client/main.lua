-- chikun :: 2014
-- Networking test: Client

-- Require socket libraries
local socket = require "socket"

-- Address and port of the server
local address, port = "funkeh.no-ip.org", 3564

function love.load()

    -- Data list
    tabData = { }

    -- UDP functions
    udp = socket.udp()

    -- Entity ID
    id = 0

    -- Timer
    timer = 0

    -- Set timeout and port number
    udp:settimeout(0)
    udp:setpeername(address, port)

    -- Get entity ID
    udp:send("getID 0 0")

    -- Self
    self = {
        x = 32,
        y = 32,
        xPrevious = 32,
        yPrevious = 32
    }

    -- Others
    others = { }

end

function love.update(dt)

    -- *Move
    local xMove, yMove = 0, 0

    -- Move player if can
    if love.keyboard.isDown("left") then xMove = -1 end
    if love.keyboard.isDown("right") then xMove = xMove + 1 end
    if love.keyboard.isDown("up") then yMove = -1 end
    if love.keyboard.isDown("down") then yMove = yMove + 1 end
    self.x = math.floor(self.x + xMove * 128 * dt + 0.5)
    self.y = math.floor(self.y + yMove * 128 * dt + 0.5)


    -- Get update
    udp:send("update " .. id .. " 0")

    -- UDP receive data loop
    repeat

        -- Receive data
        data, msg = udp:receive()

        -- If we have data...
        if data then

            cmd, parms = data:match("^(%S*) (.*)")

            love.window.setTitle(cmd)

            -- If entity ID isn't set..
            if cmd == "setID" then

                -- .. set it
                id = tonumber(parms)

            elseif cmd == "move" then

                eID, eX, eY = parms:match("(.*) (.*) (.*)")

                local idFound = false

                for k, v in ipairs(others) do

                    if eID == v.id then

                        idFound = true

                        v.x, v.y = eX, eY

                    end

                end

                if not idFound then

                    table.insert(others, {
                        id = eID,
                        x = eX,
                        y = eY
                        })

                end

            end

        end

    -- If there is no data, end loop
    until not data


    -- Increase timer
    timer = timer + dt

    -- Send update data every 1/10th a second
    if timer >= 0.1 then

        -- Decrease timer
        timer = timer - 0.1

        -- If player has moved, send movement data
        if self.x ~= self.xPrevious or self.y ~= self.yPrevious then

            -- Update movement
            udp:send("move " .. id .. " " ..
                self.x .. " " .. self.y)

            -- Update previous position
            self.xPrevious = self.x
            self.yPrevious = self.y

        end

    end

end

function love.draw()

    -- Draw others
    for k, v in ipairs(others) do
        love.graphics.setColor(255, 128, 0)
        love.graphics.rectangle("fill", v.x, v.y, 32, 32)
    end

    -- Draw self
    love.graphics.setColor(0, 255, 128)
    love.graphics.rectangle("fill", self.x, self.y, 32, 32)

    love.graphics.setColor(255, 255, 255)
    love.graphics.print(id, 16, 16)

end

function love.quit()

    -- Close sockets on game close
    udp:close()

end
